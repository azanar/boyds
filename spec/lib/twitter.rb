require 'spec_helper'
require 'twitter'

describe Twitter do
  FolloweeRequest = Twitter::FolloweeRequest
  FolloweePaginator = Twitter::FolloweePaginator
  context ".url_for_auth" do
    it 'should call twitter and get a url' do
      #$stderr.puts Twitter.url_for_auth "http://boyds.heroku.com/o_auth/process_callback"
    end
  end
  context "#from_oauth_token" do
    it 'should get an access token for the user and create a new Twitter instance' do
      #$stderr.puts Twitter.from_oauth_token("VAypCbWrf8VtXX6HMg9GJ7zkj1iytP5PcmHEQWCfOTA", "Oz6iZ9AdLBZK7lgWIpaI3BFDLqjnpoqIxsPft8tMYM").inspect
  end
end
context ".followees" do
  it 'should call into twitter and return a list of followees' do
    $stderr.puts Twitter.new("16519633-4OqYD6e896ro0qHNtLxr2m7Hvaag86t2dBns6DIH4", "JgST8FDTPDYVCRwul4DxkbGG8HeG8exBbTx2lvmB0").followees(1).inspect
  end
end
describe FolloweePaginator do
  context ".request" do
    it 'should return nothing if no ids are returned' do
      mock_request = mock(Twitter::FolloweeRequest)
      mock_request.should_receive(:execute).and_return({"next_cursor_str" => "0", "ids" => []})
      FolloweeRequest.should_receive(:new).with("-1", nil).and_return(mock_request)

      pager = FolloweePaginator.new(1,nil)
      res = pager.get
      res.should == []
    end

    it 'should return a list of ids when they will all fit on one page' do
      mock_request = mock(FolloweeRequest)
      mock_request.should_receive(:execute).and_return({"next_cursor_str" => "0", "ids" => 1.upto(30).to_a})
      FolloweeRequest.should_receive(:new).with("-1", nil).and_return(mock_request)

      pager = FolloweePaginator.new(1,nil)
      pager.get.should == 1.upto(20).to_a
    end

    it 'should return a list of ids when they split across multiple pages' do
      mock_request = mock(FolloweeRequest)
      mock_request.should_receive(:execute).and_return({"next_cursor_str" => "12345", "ids" => 1.upto(10).to_a})
      FolloweeRequest.should_receive(:new).with("-1",nil).and_return(mock_request)

      mock_request2 = mock(FolloweeRequest)
      mock_request2.should_receive(:execute).and_return({"next_cursor_str" => "0", "ids" => 11.upto(30).to_a})
      FolloweeRequest.should_receive(:new).with("12345",nil).and_return(mock_request2)

      pager = FolloweePaginator.new(1,nil)
      pager.get.should == 1.upto(20).to_a

    end

    it 'should return a list of ids when they are all on a subsequent page' do
      mock_request = mock(FolloweeRequest)
      mock_request.should_receive(:execute).and_return({"next_cursor_str" => "12345", "ids" => 1.upto(10).to_a})
      FolloweeRequest.should_receive(:new).with("-1",nil).and_return(mock_request)

      mock_request2 = mock(FolloweeRequest)
      mock_request2.should_receive(:execute).and_return({"next_cursor_str" => "0", "ids" => 11.upto(60).to_a})
      FolloweeRequest.should_receive(:new).with("12345",nil).and_return(mock_request2)

      pager = FolloweePaginator.new(2,nil)
      pager.get.should == 21.upto(40).to_a
    end

    it 'should return a list of ids when they split across multiple pages' do
      mock_request = mock(FolloweeRequest)
      mock_request.should_receive(:execute).and_return({"next_cursor_str" => "12345", "ids" => 1.upto(30).to_a})
      FolloweeRequest.should_receive(:new).with("-1",nil).and_return(mock_request)

      mock_request2 = mock(FolloweeRequest)
      mock_request2.should_receive(:execute).and_return({"next_cursor_str" => "0", "ids" => 31.upto(40).to_a})
      FolloweeRequest.should_receive(:new).with("12345",nil).and_return(mock_request2)

      pager = FolloweePaginator.new(2,nil)
      pager.get.should == 21.upto(40).to_a

    end

    it 'should return a list of whatever ids it can find when there are not enough' do
      mock_request = mock(FolloweeRequest)
      mock_request.should_receive(:execute).and_return({"next_cursor_str" => "0", "ids" => 1.upto(10).to_a})
      FolloweeRequest.should_receive(:new).with("-1",nil).and_return(mock_request)

      pager = FolloweePaginator.new(1,nil)
      pager.get.should == 1.upto(10).to_a

    end
  end
end
end
