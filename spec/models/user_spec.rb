require 'spec_helper'

require 'twitter'

describe User do
  context "#create_from_twitter" do
    it 'should create a user from twitter when an twitter object is passed' do
      mock_token = {:token => "foo", :secret => "bar"}
      mock_twitter = mock(Twitter)
      mock_twitter.should_receive(:access_token).twice.and_return(mock_token)

      Twitter.should_receive(:new).with(mock_token[:token], mock_token[:secret]).and_return(mock_twitter)
      mock_twitter.should_receive(:followees).and_return([])

      u = User.create_from_twitter(mock_twitter)

      User.where(:access_token => "foo").count.should == 1
    end

    it 'should fail if the access token is nil' do
      mock_twitter = mock(Twitter)
      mock_twitter.should_receive(:access_token).and_return(nil)

      expect { 
        User.create_from_twitter(mock_twitter)
      }.to raise_error
    end
  end

  context ".followees" do
    it 'should call down to the twitter lib object' do
      u = FactoryGirl.create(:user, :access_token => "dummy_token", :secret => "dummy_secret")

      mock_twitter = mock(Twitter)

      u.followees(1)

      u.destroy
    end
  end

  context ".unfollow" do
    it 'should call down to the twitter lib object' do
      u = FactoryGirl.create(:user, :access_token => "dummy_token", :secret => "dummy_secret")
      f = FactoryGirl.create(:friend, :user_id => u.id, :screen_name => "foo", :twitter_id => "12345") 

      mock_twitter = mock(Twitter)

      mock_twitter_user = Twitter::User.new("12345", "foo", "name")

      Twitter.should_receive(:new).with("dummy_token", "dummy_secret").and_return(mock_twitter)

      mock_twitter.should_receive(:unfollow).with(1).and_return([mock_twitter_user])

      u.unfollow(1)

      u.destroy
    end
  end

  context ".follow" do
    it 'should call down to the twitter lib object' do
      u = FactoryGirl.create(:user, :access_token => "dummy_token", :secret => "dummy_secret")

      mock_twitter = mock(Twitter)

      mock_twitter_user = Twitter::User.new("id", "screen", "name")

      Twitter.should_receive(:new).with("dummy_token", "dummy_secret").and_return(mock_twitter)

      mock_twitter.should_receive(:follow).with(1).and_return([mock_twitter_user])

      u.follow(1)

      u.destroy
    end
  end

  context ".search" do
    it 'should call down to the twitter lib object' do
      u = FactoryGirl.create(:user, :access_token => "dummy_token", :secret => "dummy_secret")

      mock_twitter = mock(Twitter)

      Twitter.should_receive(:new).with("dummy_token", "dummy_secret").and_return(mock_twitter)

      mock_twitter.should_receive(:search).with(1)

      u.search(1)

      u.destroy
    end
  end

  context ".end_session" do
    it 'should call down to the twitter lib object' do
      u = FactoryGirl.create(:user, :access_token => "dummy_token", :secret => "dummy_secret")

      mock_twitter = mock(Twitter)

      Twitter.should_receive(:new).with("dummy_token", "dummy_secret").and_return(mock_twitter)

      mock_twitter.should_receive(:end_session)

      u.end_session

      u.destroy
    end
  end
end
