require 'spec_helper'

describe OAuthController do
  describe "GET login" do
    it 'should redirect to twitter if we are not authenticated' do
      Twitter.should_receive(:url_for_auth).and_return("http://api.twitter.com/oauth/authorize")

      get :login
      response.should redirect_to "http://api.twitter.com/oauth/authorize"

    end

    it 'should redirect to twitter if we claim to be authenticated, but are not' do
      Twitter.should_receive(:url_for_auth).and_return("http://api.twitter.com/oauth/authorize")
      get :login, nil, {:user_id => -1}
      response.should redirect_to "http://api.twitter.com/oauth/authorize"
    end

    it 'should redirect to our user page if we are' do
      u = FactoryGirl.create(:user, :access_token => "blarg", :secret => "blaz")
      get :login, nil, {:user_id => u.id}
      response.should redirect_to :controller => "user", :action => "index" 
      #u.destroy
    end
  end
  describe "POST process_callback" do
    it 'should store a user record when thrown an oauth token and verifier' do
      u1 = FactoryGirl.create(:user, :access_token => "blarg", :secret => "blaz")
      mock_twitter = mock(Twitter)
      mock_token = {:token => "foo", :secret => "bar"}

      mock_twitter.should_receive(:access_token).twice.and_return(mock_token)
      mock_twitter.should_receive(:followees).and_return([])

      Twitter.should_receive(:from_oauth_token).and_return(mock_twitter)
      Twitter.should_receive(:new).with(mock_token[:token], mock_token[:secret]).and_return(mock_twitter)

      post :process_callback
      u = User.where(:access_token => "foo").all
      u.length.should == 1
      u.each {|v| v.destroy}
    end
  end
  describe "POST logout" do
    it 'should log a user out if they are logged in' do
      u = FactoryGirl.create(:user, :access_token => "12345", :secret => "54321")
      session[:user_id] = u.id
      mock_twitter = mock(Twitter)
      mock_twitter.should_receive(:end_session)
      Twitter.should_receive(:new).with("12345", "54321").and_return(mock_twitter)
      post :logout
      session[:user_id].should be_nil 
      response.should redirect_to :action => "index"
    end

    it 'should do nothing and redirect if there is no user logged in' do
      post :logout
      response.should redirect_to :action => "index"
    end
  end
end
