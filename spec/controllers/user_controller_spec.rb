require 'spec_helper'

describe UserController do
  before(:each) do
    @user = FactoryGirl.create(:user, :access_token => "foo", :secret => "bar")
  end
  describe "GET index" do
    it 'should ask for and provide followees to the view' do
      followees = 5.times.map do |x|
        FactoryGirl.create(:friend, :user => @user, :twitter_id => "#{x}", :screen_name => "screen#{x}")
      end
      get :index, {}, { :user_id => @user.id }
      response.status.should == 200
      assigns(:u).should == @user
      assigns(:twits).should == {"followees" => followees, "pages" => 1}

  end

  it 'should redirect the user if they are not valid' do
    get :index, nil, {:user_id => -1}
    response.should redirect_to :controller => "o_auth", :action => "index"
  end
end
describe "GET refresh" do
  it 'should refresh when asked' do
    mock_users = 5.times.map do |x|
      Twitter::User.new(x, "screen#{x}", "user#{x}")
    end
    mock_twitter = mock(Twitter)
    mock_twitter.should_receive(:followees).and_return(mock_users)
    Twitter.should_receive(:new).with("foo","bar").and_return(mock_twitter)
    get :refresh, {}, { :user_id => @user.id }
  end
end
describe "POST follow" do
  it 'should submit follows and then redirect' do
    mock_users = 5.times.map do |x|
      Twitter::User.new(x, "screen#{x}", "user#{x}")
    end
    mock_twitter = mock(Twitter)
    mock_twitter.should_receive(:follow).and_return(mock_users)
    Twitter.should_receive(:new).with("foo","bar").and_return(mock_twitter)
    post :follow, {:act => [1,2,3,4,5]}, {:user_id => @user.id}
    response.should redirect_to :action => "index", :page => 1
  end
  it 'should redirect the user if they are not valid' do
    post :follow, {:act => [1,2,3,4,5]}, {:user_id => -1}
    response.should redirect_to :controller => "o_auth", :action => "index"
  end
end
describe "POST unfollow" do
  it 'should submit unfollows and then redirect' do
    mock_users = 5.times.map do |x|
      Twitter::User.new(x, "screen#{x}", "user#{x}")
    end
    mock_twitter = mock(Twitter)
    mock_twitter.should_receive(:unfollow).and_return(mock_users)
    Twitter.should_receive(:new).with("foo","bar").and_return(mock_twitter)
    post :unfollow, {:act => [1,2,3,4,5]}, {:user_id => @user.id}
    response.should redirect_to :action => "index", :page => 1
  end
  it 'should redirect the user if they are not valid' do
    post :follow, {:act => [1,2,3,4,5]}, {:user_id => -1}
    response.should redirect_to :controller => "o_auth", :action => "index"
  end

end
describe "GET search" do
  it 'should submit unfollows and then redirect' do
    mock_users = 5.times.map do |x|
      Twitter::User.new(x, "screen#{x}", "user#{x}")
    end
    mock_twitter = mock(Twitter)
    mock_twitter.should_receive(:search).with("foo").and_return(mock_users)
    Twitter.should_receive(:new).with("foo","bar").and_return(mock_twitter)
    get :search, {:term => "foo"}, {:user_id => @user.id}
    assigns(:u).should == @user
    assigns(:found_twits).should == mock_users
  end
  it 'should redirect the user if they are not valid' do
    post :follow, {:act => [1,2,3,4,5]}, {:user_id => -1}
    response.should redirect_to :controller => "o_auth", :action => "index"
  end

end
end
