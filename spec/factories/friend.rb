FactoryGirl.define do
  factory :friend do
    user_id 1
    screen_name "foo"
    twitter_id "bar"
  end
end
