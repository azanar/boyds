class CreateFriends < ActiveRecord::Migration
  def change
    create_table :friends do |t|
      t.integer :id
      t.integer :twitter_id
      t.text :screen_name
      t.references :user

      t.timestamps
    end
  end
end
