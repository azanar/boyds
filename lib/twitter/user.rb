class Twitter
class User

  def self.initialize_from_twitter_user_arr(arr)
    arr.map { |u| self.initialize_from_twitter_user(u) }
  end

  def self.initialize_from_twitter_user(u)
    Twitter::User.new(u["id_str"], u["screen_name"], u["name"]) 
  end
  attr_reader :twitter_id, :screen_name, :name
  def initialize(twitter_id, screen_name, name)
    @twitter_id = twitter_id
    @screen_name = screen_name
    @name = name
  end
end
end
