require 'json'
require 'faraday'
require 'simple_oauth'

require 'twitter/user'

class Twitter

  class TwitterAuthError < StandardError ; end

  def self.url_for_auth(callback_url)
    oauth = SimpleOAuth::Header.new(:post, "https://api.twitter.com/oauth/request_token", nil,
                                    {:consumer_key => Rails.configuration.twitter_key, 
                                      :consumer_secret => Rails.configuration.twitter_secret,
                                      :callback => callback_url
    })

    connection = Faraday::Connection.new "https://api.twitter.com/"
    Rails.logger.info "pinging api with the following authorization #{oauth.to_s}"

    resp = connection.post "/oauth/request_token" do |req|
      req.headers["Authorization"] = oauth.to_s
    end

    if resp.status != 200
      Rails.logger.error "Got unhappy response from Twitter.\n\nStatus: #{resp.status}\n\nMessage:#{resp.body}\nEND\n"
      raise "Something went sideways in Twitter"
    end

    hash = CGI.parse(resp.body)

    URI.parse("https://api.twitter.com/oauth/authenticate?oauth_token=#{hash["oauth_token"][0]}")

  end


  def self.from_oauth_token(oauth_token, oauth_verifier)
    resp = AccessTokenRequest.new(oauth_token, oauth_verifier, {:token => oauth_token }).execute

    Rails.logger.info "Got response for Access Token as #{resp.inspect}"
    access_token = resp["oauth_token"][0]
    access_token_secret = resp["oauth_token_secret"][0]

    Twitter.new(access_token, access_token_secret)
  end

  attr_reader :access_token

  def initialize(access_token, access_token_secret)
    @access_token = {:token => access_token, :secret => access_token_secret}
  end

  class TwitterAPIRequest

    SERVICE_URL = "https://api.twitter.com"

    def initialize(token)
      #@connection = Faraday::Connection.new "http://127.0.0.1:8000" #SERVICE_URL
      @connection = Faraday::Connection.new SERVICE_URL
      @access_token = token[:token] # ugh, would really appreciate inner classes right about now.
      @access_token_secret = token[:secret]
    end

    def request
      resp = @connection.run_request method, path, nil, nil do |req|
        req.headers["Authorization"] = get_auth_header
        if block_given?
          yield req
        end
      end
      handle_response(resp)
    end

    def handle_response(resp)
      if resp.status == 401
        Rails.logger.warn "Twitter token revoked. Message: #{resp.body}"
        raise TwitterAuthError.new("Twitter has revoked the access token for this user. Please reauthenticate.")
      elsif resp.status != 200
        raise "Something has gone wrong with the request to Twitter. Please try again later.\n\nStatus: #{resp.status}\n\nMessage: #{resp.body}"
      end
      if resp.body 
        parse(resp.body)
      end
    end

    def parse(body)
      JSON.parse(body)
    end

    def execute
      request do |req|
        req.params = params
      end
    end

    private

    def get_auth_header
      s = SimpleOAuth::Header.new(method, SERVICE_URL + path, params, 
                              { :consumer_key => Rails.configuration.twitter_key, 
                                :consumer_secret => Rails.configuration.twitter_secret,
                                :token => @access_token,
                                :token_secret => @access_token_secret
                              })
      s.to_s
    end
  end

  class EndSessionRequest < TwitterAPIRequest
    def initialize(token)
      super(token)
    end
    def path
      '/1/account/end_session.json'
    end
    def params
      nil
    end
    def method
      :post
    end
    def execute
      request
    end
  end

  def end_session
    EndSessionRequest.new(@access_token).execute
  end

  class AccessTokenRequest < TwitterAPIRequest
    def initialize(oauth_token, oauth_verifier, token)
      @oauth_token = oauth_token
      @oauth_verifier = oauth_verifier
      super(token)
    end

    def parse(body)
      CGI.parse(body)
    end

    def path
      '/oauth/access_token'
    end

    def params
      {"oauth_token"=>@oauth_token, "oauth_verifier"=>@oauth_verifier}
    end

    def method
      :get
    end

    def execute
      request do |req|
        req.body = params
      end
    end

  end
  class FolloweeRequest < TwitterAPIRequest

    def initialize(cursor, token)
      @cursor = cursor
      super(token)
    end

    def path
      '/1/friends/ids.json'
    end

    def params
      {"cursor" => @cursor}
    end

    def handle_response(resp)
      if resp.status == 404
        # wow, really Twitter? A lack of followers means the friends endpoint throws a 404? So, what should I see if/when the endpoint actually ceases to exist?
        EmptyFolloweeResponse.new
      else
        super(resp)
      end
    end

    def parse(data)
      resp = super(data)
      FolloweeResponse.new(resp, @access_token)
    end

    def method
      :get
    end
  end

  class FolloweeResponse
    attr_reader :ids
    attr_reader :next_req
    attr_reader :prev_req

    def initialize(resp, token)
      @token = token
      @ids = resp["ids"]
      @prev_req = request_for_cursor(resp["previous_cursor_str"])
      @next_req = request_for_cursor(resp["next_cursor_str"])
    end

    def next_page
      if @next_req
        @next_req.execute
      end
    end

    def prev_page
      if @prev_req
        @prev_req.execute
      end
    end

    private

    def request_for_cursor(cursor)
      if cursor == "0" || cursor == "-1"
        nil
      else
        FolloweeRequest.new(cursor, @token)
      end
    end
  end
  #
  # This exists to deal with Twitter throwing 404s when someone merely has no contacts.
  class EmptyFolloweeResponse < Twitter::FolloweeResponse

    def initialize
      @ids = []
      @next_req = @prev_req = nil
    end

  end

  class FolloweeCollector

    attr_reader :followees

    def initialize(access_token)
      @access_token = access_token
      @followees = collect_followees
    end

    private

    def collect_followees
      ids = []
      req = FolloweeRequest.new("-1", @access_token)
      while !req.nil?
        puts "#{req}"
        res = req.execute
        ids += res.ids
        req = res.next_req
      end
      ids
    end
  end

  def followees_as_ids
    FolloweeCollector.new(@access_token).followees
  end

  # TODO: This method is a little hacky. Think about splitting it into two different methods.
  def followees
    ids = followees_as_ids
    ids.each_slice(1000).map do |id_slice|
      u = UserLookupRequest.new(id_slice, @access_token).execute
      Twitter::User.initialize_from_twitter_user_arr(u)
    end.flatten
  end

  class UserLookupRequest < TwitterAPIRequest
    def initialize(ids,token)
      @ids = ids
      super(token)
    end

    def path
      '/1/users/lookup.json'
    end

    def method
      :post
    end

    def params
      id_str = @ids.join(",")
      {:user_id => id_str}
    end

    def execute
      request do |req|
        req.body = params
      end
    end
  end
  
  class FollowRequest < TwitterAPIRequest
    def initialize(tid, token)
      @tid = tid
      super(token)
    end

    def path
      '/1/friendships/create.json'
    end

    def method
      :post
    end

    def params
      {:user_id => @tid}
    end

    def execute
      request do |req|
        req.body = params
      end
    end
  end

  def follow(tids)
    followed_users = tids.map do |tid|
      FollowRequest.new(tid, @access_token).execute
    end
    Twitter::User.initialize_from_twitter_user_arr(followed_users)
  end

  class UnfollowRequest < TwitterAPIRequest
    def initialize(tid, token)
      @tid = tid
      super(token)
    end

    def path
      '/1/friendships/destroy.json'
    end

    def method
      :post
    end

    def params
      {:user_id => @tid}
    end

    def execute
      request do |req|
        req.body = params
      end
    end
  end

  def unfollow(tids)
    unfollowed_users = tids.map do |tid|
      UnfollowRequest.new(tid, @access_token).execute
    end
    Twitter::User.initialize_from_twitter_user_arr(unfollowed_users)
  end

  def search(q)
    Rails.logger.info "Searching for #{q}"
    Twitter::User.initialize_from_twitter_user_arr([SearchRequest, SearchUsersRequest].inject([]) {|result, method|
      result + method.new(q,@access_token).execute
    })
  end

  class SearchRequest < TwitterAPIRequest
    def initialize(q, token)
      @q = q
      super(token)
    end

    def path
      '/1/users/search.json'
    end

    def method
      :get
    end

    def params
      {:q => @q}
    end

    def execute
      resp = request do |req|
        req.params = params
      end
    end
  end

  class SearchUsersRequest < TwitterAPIRequest
    def initialize(q, token)
      @q = q
      super(token)
    end

    def path
      '/1/users/search.json'
    end

    def method
      :get
    end

    def params
      {:q => @q}
    end

    def execute
      resp = request do |req|
        req.params = params
      end
    end
  end
end
