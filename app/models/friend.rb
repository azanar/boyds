class Friend < ActiveRecord::Base
  attr_accessible :twitter_id, :screen_name, :user_id
  belongs_to :user
end
