require 'twitter'

class User < ActiveRecord::Base
  attr_accessible :access_token, :secret
  validates_presence_of :access_token, :secret

  has_many :friends do
    def create_from_twitter_user(twitter)
      create(:screen_name => twitter.screen_name, :twitter_id => twitter.twitter_id)
    end

    def destroy_from_twitter_user(twitterer)
      where(:twitter_id => twitterer.twitter_id).destroy
    end

    def create_from_twitter_user_arr(twitters)
      twitters.map do |t|
        create_from_twitter_user(t)
      end
    end

    def destroy_from_twitter_user_arr(twitters)
      twitters.map do |t|
        create_from_twitter_user(t)
      end
    end
  end

  def self.create_from_twitter(t)
    u = User.create!(:access_token => t.access_token[:token], :secret => t.access_token[:secret])
    u.update_followees
    u
  end

  def update_followees
    friends.destroy_all
    friends.create_from_twitter_user_arr(twitter.followees)
  end

  FOLLOWEES_PER_PAGE = 20.0 
  def followees(page)
    offset = (page - 1) * FOLLOWEES_PER_PAGE
    count = friends.count
    logger.info "#{count / FOLLOWEES_PER_PAGE}"
    {
      :followees => friends.limit(FOLLOWEES_PER_PAGE).offset(offset).order("twitter_id"),
      :pages => (count / FOLLOWEES_PER_PAGE).ceil
    }
  end

  def unfollow(ids)
    unfollowed = twitter.unfollow(ids)
    friends.destroy_from_twitter_user_arr(unfollowed)
  end

  def follow(ids)
    new_followees = twitter.follow(ids)
    friends.create_from_twitter_user_arr(new_followees)
  end

  def search(term)
    twitter.search(term)
  end

  def end_session
    twitter.end_session
  end

  private

  def twitter
    @twitter ||= Twitter.new(self.access_token, self.secret)
  end

end
