require 'twitter'
class OAuthController < ApplicationController
  def login
    if !session[:user_id].blank?
      if User.where(:id => session[:user_id]).exists? 
        logger.debug "user is already logged in as id #{session[:user_id]}"
        redirect_to :controller => "user", :action => "index"
        return
      else
        logger.error "user #{session[:user_id]} doesn't exist. Purging and taking the user through the usual auth path."
        session[:user_id] = nil
      end
    end

    callback_url = url_for(:action => "process_callback")

    url = Twitter.url_for_auth(callback_url)
    logger.info url

    redirect_to url.to_s
  rescue StandardError => e
    logger.error "Something went wrong getting the auth url #{e}"
    redirect_to :controller => "o_auth", :action => "error"
  end

  def process_callback
    t = Twitter.from_oauth_token(params[:oauth_token], params[:oauth_verifier])
    u = User.create_from_twitter(t)

    session[:user_id] = u.id
    logger.info("Assigned user id #{session[:user_id]} to session.")

    redirect_to :controller => "user", :action => "index"
  end

  def logout
    if !session[:user_id].blank?
      u = User.find(session[:user_id])
      u.end_session
    end
    session[:user_id] = nil
    redirect_to :action => "index"
  end
end
