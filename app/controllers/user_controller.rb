class UserController < ApplicationController
  before_filter :attach_user

  def index
    redirect_on_auth_failure do
      @page = if params.has_key?(:page) and !params[:page].nil? and params[:page].to_i > 0
                params[:page].to_i
              else
                1
              end
      @twits = @u.followees(@page)
    end
  end

  def refresh
    redirect_on_auth_failure do
      @u.update_followees
      redirect_to :action => "index", :page => 1
    end
  end

  def follow
    redirect_on_auth_failure do
      @u.follow(params[:act])
      redirect_to :action => "index", :page => 1
    end
  end

  def unfollow
    redirect_on_auth_failure do
      @u.unfollow(params[:act])
      redirect_to :action => "index", :page => 1
    end
  end

  def search
    redirect_on_auth_failure do
      @found_twits = @u.search(params[:term])
    end
  end

  private

  def redirect_on_auth_failure(&block)
    yield block
  rescue Twitter::TwitterAuthError => e
    @u.destroy
    flash[:error] = e.message
    redirect_to :controller => "o_auth", :action => "index"
  end


  def attach_user
    if session[:user_id] == nil 
      logger.warn "No user_id provided. Redirecting to auth page."
      flash[:error] = "You have not logged in yet. Please log in through Twitter."
      redirect_to :controller => "o_auth", :action => "index"
      return
    end
    @u = User.where(:id => session[:user_id]).first
    if @u.nil?
      logger.error "User #{session[:user_id]} does not exist. Redirecting back to oauth index."
      flash[:error] = "You have not logged in yet. Please log in through Twitter."
      redirect_to :controller => "o_auth", :action => "index"
      return
    end
  end
  def page
    if params.has_key?(:page)
      params[:page].to_i
    else
      0
    end
  end
end
